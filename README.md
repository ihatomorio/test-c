# C言語test用リポジトリ

## types: 型の長さ確認用

### usage

``` here.sh
gcc types.c
./a.out
```

### 環境構築
```shell for WSL
sudo apt install doxygenv
sudo apt install graphviz
```

### ドキュメントの自動生成
```shell for all environ.
make docs
```

### ドキュメントの参照
docs/html/index.htmlをブラウザなどで参照してください。
