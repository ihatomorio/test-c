#include <stdio.h>

int main(int argc, char const *argv[])
{
    int number = 1;
    for(;1;number++){
        if( number > 30 ){
            printf("\n");
            break;
        }

        if( (number % 3 != 0) && (number % 5 != 0) ){
            printf("%d, ", number);
            continue;
        }

        if( number % 3 == 0){
            printf("Fizz");
        }
        if( number % 5 == 0){
            printf("Buzz");
        }
        printf(", ");
    }

    return 0;
}
