#include <stddef.h>
#include <string.h>

int somefunc_1(int a, int b){
    return a+b;
}

int hello_string(char* buf, size_t buf_len){
    strncpy( buf, "Hello, World!", buf_len);

    return 0;
}