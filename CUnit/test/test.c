#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdint.h>
#include <limits.h>

#include "../module.h"

// 別ディレクトリのリンクしたモジュールのテストをするためのテストプログラムのサンプル

void test_11(void){
    CU_ASSERT( somefunc_1(0, 0) == 0);
}

void test_12(void){
    CU_ASSERT( somefunc_1(1, 0) == 1);
}

void test_13(void){
    CU_ASSERT( somefunc_1(0, 1) == 1);
}

void test_14(void){
    CU_ASSERT( somefunc_1(INT_MAX, 0) == INT_MAX);
}

void shouldOnePlusTwoIsThree(void){
    CU_ASSERT_EQUAL( somefunc_1(1, 2) , 3);
}

int add_test_suite_1(void){
    CU_pSuite test_suite = NULL;
    int return_code = 0;

    test_suite = CU_add_suite("suite1", NULL, NULL);

    if( test_suite == NULL){
        printf("adding failed.\n");
        return_code = 1;
        goto final;
    }

    CU_ADD_TEST(test_suite, test_11);
    CU_ADD_TEST(test_suite, test_12);
    CU_ADD_TEST(test_suite, test_13);
    CU_ADD_TEST(test_suite, test_14);
    CU_ADD_TEST(test_suite, shouldOnePlusTwoIsThree);

final:
    return return_code;
}

void test_2(void){
    char buf[256];
    hello_string(buf, 256);
    CU_ASSERT_STRING_EQUAL(buf, "Hello, World!");
}

int add_test_suite_2(void){
    CU_pSuite test_suite = NULL;
    int return_code = 0;

    test_suite = CU_add_suite("suite2", NULL, NULL);

    if( test_suite == NULL){
        printf("adding failed.\n");
        return_code = 1;
        goto final;
    }

    CU_ADD_TEST(test_suite, test_2);

final:
    return return_code;
}

int main(int argc, char const *argv[])
{
    unsigned int number_of_tests_failed = 0;
    int return_code = 0;

    CU_initialize_registry();

    add_test_suite_1();
    add_test_suite_2();

    // use CU_basic_run_tests in order to CU_console_run_tests
    CU_basic_run_tests();

    number_of_tests_failed = CU_get_number_of_tests_failed();

    if( 0 != number_of_tests_failed ){
        return_code = 2;
        goto final;
    }

final:
    CU_cleanup_registry();
    return return_code;
}
