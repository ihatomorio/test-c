#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char const *argv[])
{
    int exit_code = EXIT_SUCCESS;
    FILE *file_stream = fopen("./hoge.toml", "r");
    if (file_stream == NULL)
    {
        exit_code = EXIT_FAILURE;
        goto final;
    }
    size_t buffer_bytes;
    char *line_buffer_null_termed = (char *)malloc(buffer_bytes);
    if (line_buffer_null_termed == NULL)
    {
        exit_code = EXIT_FAILURE;
        goto final;
    }
    uint line = 0;
    while (true)
    {
        ssize_t bytes_read = getline(&line_buffer_null_termed, &buffer_bytes, file_stream);
        if (bytes_read == -1)
        {
            printf("\n");
            break;
        }
        printf("%d: %s", line, line_buffer_null_termed);
        printf("%d: ", line);
        int i = 0;
        for (i = 0; i < bytes_read; i++)
        {
            if (line_buffer_null_termed[i] == '\r')
            {
                printf("CR, ");
            }
            else if (line_buffer_null_termed[i] == '\n')
            {
                printf("LF, ");
            }
            else
            {
                printf("%c, ", (char)line_buffer_null_termed[i]);
            }
        }
        printf("\n");
        line++;
    }
final:
    if (file_stream != NULL)
    {
        fclose(file_stream);
    }
    free(line_buffer_null_termed);
    return exit_code;
}
