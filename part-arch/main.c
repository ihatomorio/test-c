#include <stdio.h>

#ifdef __linux__
  #define HELLO_STRING "Hello, Linux!"
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #ifdef TARGET_OS_OSX
        #define HELLO_STRING "Hello, OSX!"
    #else
        #define HELLO_STRING "Hello, Apple!"
    #endif
#else
    #define HELLO_STRING "Hello, Unknown!"
#endif

int main(int argc, char const *argv[])
{
    printf("%s\n", HELLO_STRING);
    return 0;
}
