--- diff-gcc.txt	2021-07-05 23:20:15.403168383 +0900
+++ diff-clang.txt	2021-07-05 23:20:13.579170716 +0900
@@ -7,22 +7,20 @@
 #define __ATOMIC_ACQ_REL 4
 #define __ATOMIC_ACQUIRE 2
 #define __ATOMIC_CONSUME 1
-#define __ATOMIC_HLE_ACQUIRE 65536
-#define __ATOMIC_HLE_RELEASE 131072
 #define __ATOMIC_RELAXED 0
 #define __ATOMIC_RELEASE 3
 #define __ATOMIC_SEQ_CST 5
-#define __attribute_alloc_size__(params) __attribute__ ((__alloc_size__ params))
-#define __attribute_artificial__ __attribute__ ((__artificial__))
+#define __attribute_alloc_size__(params) 
+#define __attribute_artificial__ 
 #define __attribute_const__ __attribute__ ((__const__))
-#define __attribute_copy__(arg) __attribute__ ((__copy__ (arg)))
+#define __attribute_copy__(arg) 
 #define __attribute_deprecated__ __attribute__ ((__deprecated__))
 #define __attribute_deprecated_msg__(msg) __attribute__ ((__deprecated__ (msg)))
 #define __attribute_format_arg__(x) __attribute__ ((__format_arg__ (x)))
 #define __attribute_format_strfmon__(a,b) __attribute__ ((__format__ (__strfmon__, a, b)))
 #define __attribute_malloc__ __attribute__ ((__malloc__))
 #define __attribute_noinline__ __attribute__ ((__noinline__))
-#define __attribute_nonstring__ __attribute__ ((__nonstring__))
+#define __attribute_nonstring__ 
 #define __attribute_pure__ __attribute__ ((__pure__))
 #define __attribute_used__ __attribute__ ((__used__))
 #define __attribute_warn_unused_result__ __attribute__ ((__warn_unused_result__))
@@ -37,64 +35,54 @@
 #define __BLKSIZE_T_TYPE __SYSCALL_SLONG_TYPE
 #define __bos0(ptr) __builtin_object_size (ptr, 0)
 #define __bos(ptr) __builtin_object_size (ptr, __USE_FORTIFY_LEVEL > 1)
-#define _BSD_SIZE_T_ 
-#define _BSD_SIZE_T_DEFINED_ 
 #define BUFSIZ 8192
 #define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
-#define __CET__ 3
-#define __CHAR16_TYPE__ short unsigned int
+#define __CHAR16_TYPE__ unsigned short
 #define __CHAR32_TYPE__ unsigned int
 #define __CHAR_BIT__ 8
+#define __clang__ 1
+#define __CLANG_ATOMIC_BOOL_LOCK_FREE 2
+#define __CLANG_ATOMIC_CHAR16_T_LOCK_FREE 2
+#define __CLANG_ATOMIC_CHAR32_T_LOCK_FREE 2
+#define __CLANG_ATOMIC_CHAR_LOCK_FREE 2
+#define __CLANG_ATOMIC_INT_LOCK_FREE 2
+#define __CLANG_ATOMIC_LLONG_LOCK_FREE 2
+#define __CLANG_ATOMIC_LONG_LOCK_FREE 2
+#define __CLANG_ATOMIC_POINTER_LOCK_FREE 2
+#define __CLANG_ATOMIC_SHORT_LOCK_FREE 2
+#define __CLANG_ATOMIC_WCHAR_T_LOCK_FREE 2
+#define __clang_major__ 10
+#define __clang_minor__ 0
+#define __clang_patchlevel__ 0
+#define __clang_version__ "10.0.0 "
 #define __CLOCKID_T_TYPE __S32_TYPE
 #define __CLOCK_T_TYPE __SYSCALL_SLONG_TYPE
-#define __code_model_small__ 1
+#define __code_model_small_ 1
 #define __CONCAT(x,y) x ## y
+#define __CONSTANT_CFSTRINGS__ 1
 #define __CPU_MASK_TYPE __SYSCALL_ULONG_TYPE
 #define __DADDR_T_TYPE __S32_TYPE
 #define __DBL_DECIMAL_DIG__ 17
-#define __DBL_DENORM_MIN__ ((double)4.94065645841246544176568792868221372e-324L)
+#define __DBL_DENORM_MIN__ 4.9406564584124654e-324
 #define __DBL_DIG__ 15
-#define __DBL_EPSILON__ ((double)2.22044604925031308084726333618164062e-16L)
+#define __DBL_EPSILON__ 2.2204460492503131e-16
 #define __DBL_HAS_DENORM__ 1
 #define __DBL_HAS_INFINITY__ 1
 #define __DBL_HAS_QUIET_NAN__ 1
 #define __DBL_MANT_DIG__ 53
 #define __DBL_MAX_10_EXP__ 308
-#define __DBL_MAX__ ((double)1.79769313486231570814527423731704357e+308L)
+#define __DBL_MAX__ 1.7976931348623157e+308
 #define __DBL_MAX_EXP__ 1024
 #define __DBL_MIN_10_EXP__ (-307)
-#define __DBL_MIN__ ((double)2.22507385850720138309023271733240406e-308L)
+#define __DBL_MIN__ 2.2250738585072014e-308
 #define __DBL_MIN_EXP__ (-1021)
-#define __DEC128_EPSILON__ 1E-33DL
-#define __DEC128_MANT_DIG__ 34
-#define __DEC128_MAX__ 9.999999999999999999999999999999999E6144DL
-#define __DEC128_MAX_EXP__ 6145
-#define __DEC128_MIN__ 1E-6143DL
-#define __DEC128_MIN_EXP__ (-6142)
-#define __DEC128_SUBNORMAL_MIN__ 0.000000000000000000000000000000001E-6143DL
-#define __DEC32_EPSILON__ 1E-6DF
-#define __DEC32_MANT_DIG__ 7
-#define __DEC32_MAX__ 9.999999E96DF
-#define __DEC32_MAX_EXP__ 97
-#define __DEC32_MIN__ 1E-95DF
-#define __DEC32_MIN_EXP__ (-94)
-#define __DEC32_SUBNORMAL_MIN__ 0.000001E-95DF
-#define __DEC64_EPSILON__ 1E-15DD
-#define __DEC64_MANT_DIG__ 16
-#define __DEC64_MAX__ 9.999999999999999E384DD
-#define __DEC64_MAX_EXP__ 385
-#define __DEC64_MIN__ 1E-383DD
-#define __DEC64_MIN_EXP__ (-382)
-#define __DEC64_SUBNORMAL_MIN__ 0.000000000000001E-383DD
-#define __DEC_EVAL_METHOD__ 2
-#define __DECIMAL_BID_FORMAT__ 1
-#define __DECIMAL_DIG__ 21
+#define __DECIMAL_DIG__ __LDBL_DECIMAL_DIG__
 #define _DEFAULT_SOURCE 1
 #define __DEV_T_TYPE __UQUAD_TYPE
 #define __ELF__ 1
 #define __END_DECLS 
 #define EOF (-1)
-#define __errordecl(name,msg) extern void name (void) __attribute__((__error__ (msg)))
+#define __errordecl(name,msg) extern void name (void)
 #define __extern_always_inline extern __always_inline __attribute__ ((__gnu_inline__))
 #define __extern_inline extern __inline __attribute__ ((__gnu_inline__))
 #define __FD_SETSIZE 1024
@@ -106,92 +94,21 @@
 #define FILENAME_MAX 4096
 #define __FINITE_MATH_ONLY__ 0
 #define __flexarr []
-#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__
-#define __FLT128_DECIMAL_DIG__ 36
-#define __FLT128_DENORM_MIN__ 6.47517511943802511092443895822764655e-4966F128
-#define __FLT128_DIG__ 33
-#define __FLT128_EPSILON__ 1.92592994438723585305597794258492732e-34F128
-#define __FLT128_HAS_DENORM__ 1
-#define __FLT128_HAS_INFINITY__ 1
-#define __FLT128_HAS_QUIET_NAN__ 1
-#define __FLT128_MANT_DIG__ 113
-#define __FLT128_MAX_10_EXP__ 4932
-#define __FLT128_MAX__ 1.18973149535723176508575932662800702e+4932F128
-#define __FLT128_MAX_EXP__ 16384
-#define __FLT128_MIN_10_EXP__ (-4931)
-#define __FLT128_MIN__ 3.36210314311209350626267781732175260e-4932F128
-#define __FLT128_MIN_EXP__ (-16381)
-#define __FLT32_DECIMAL_DIG__ 9
-#define __FLT32_DENORM_MIN__ 1.40129846432481707092372958328991613e-45F32
-#define __FLT32_DIG__ 6
-#define __FLT32_EPSILON__ 1.19209289550781250000000000000000000e-7F32
-#define __FLT32_HAS_DENORM__ 1
-#define __FLT32_HAS_INFINITY__ 1
-#define __FLT32_HAS_QUIET_NAN__ 1
-#define __FLT32_MANT_DIG__ 24
-#define __FLT32_MAX_10_EXP__ 38
-#define __FLT32_MAX__ 3.40282346638528859811704183484516925e+38F32
-#define __FLT32_MAX_EXP__ 128
-#define __FLT32_MIN_10_EXP__ (-37)
-#define __FLT32_MIN__ 1.17549435082228750796873653722224568e-38F32
-#define __FLT32_MIN_EXP__ (-125)
-#define __FLT32X_DECIMAL_DIG__ 17
-#define __FLT32X_DENORM_MIN__ 4.94065645841246544176568792868221372e-324F32x
-#define __FLT32X_DIG__ 15
-#define __FLT32X_EPSILON__ 2.22044604925031308084726333618164062e-16F32x
-#define __FLT32X_HAS_DENORM__ 1
-#define __FLT32X_HAS_INFINITY__ 1
-#define __FLT32X_HAS_QUIET_NAN__ 1
-#define __FLT32X_MANT_DIG__ 53
-#define __FLT32X_MAX_10_EXP__ 308
-#define __FLT32X_MAX__ 1.79769313486231570814527423731704357e+308F32x
-#define __FLT32X_MAX_EXP__ 1024
-#define __FLT32X_MIN_10_EXP__ (-307)
-#define __FLT32X_MIN__ 2.22507385850720138309023271733240406e-308F32x
-#define __FLT32X_MIN_EXP__ (-1021)
-#define __FLT64_DECIMAL_DIG__ 17
-#define __FLT64_DENORM_MIN__ 4.94065645841246544176568792868221372e-324F64
-#define __FLT64_DIG__ 15
-#define __FLT64_EPSILON__ 2.22044604925031308084726333618164062e-16F64
-#define __FLT64_HAS_DENORM__ 1
-#define __FLT64_HAS_INFINITY__ 1
-#define __FLT64_HAS_QUIET_NAN__ 1
-#define __FLT64_MANT_DIG__ 53
-#define __FLT64_MAX_10_EXP__ 308
-#define __FLT64_MAX__ 1.79769313486231570814527423731704357e+308F64
-#define __FLT64_MAX_EXP__ 1024
-#define __FLT64_MIN_10_EXP__ (-307)
-#define __FLT64_MIN__ 2.22507385850720138309023271733240406e-308F64
-#define __FLT64_MIN_EXP__ (-1021)
-#define __FLT64X_DECIMAL_DIG__ 21
-#define __FLT64X_DENORM_MIN__ 3.64519953188247460252840593361941982e-4951F64x
-#define __FLT64X_DIG__ 18
-#define __FLT64X_EPSILON__ 1.08420217248550443400745280086994171e-19F64x
-#define __FLT64X_HAS_DENORM__ 1
-#define __FLT64X_HAS_INFINITY__ 1
-#define __FLT64X_HAS_QUIET_NAN__ 1
-#define __FLT64X_MANT_DIG__ 64
-#define __FLT64X_MAX_10_EXP__ 4932
-#define __FLT64X_MAX__ 1.18973149535723176502126385303097021e+4932F64x
-#define __FLT64X_MAX_EXP__ 16384
-#define __FLT64X_MIN_10_EXP__ (-4931)
-#define __FLT64X_MIN__ 3.36210314311209350626267781732175260e-4932F64x
-#define __FLT64X_MIN_EXP__ (-16381)
+#define __FLOAT128__ 1
 #define __FLT_DECIMAL_DIG__ 9
-#define __FLT_DENORM_MIN__ 1.40129846432481707092372958328991613e-45F
+#define __FLT_DENORM_MIN__ 1.40129846e-45F
 #define __FLT_DIG__ 6
-#define __FLT_EPSILON__ 1.19209289550781250000000000000000000e-7F
+#define __FLT_EPSILON__ 1.19209290e-7F
 #define __FLT_EVAL_METHOD__ 0
-#define __FLT_EVAL_METHOD_TS_18661_3__ 0
 #define __FLT_HAS_DENORM__ 1
 #define __FLT_HAS_INFINITY__ 1
 #define __FLT_HAS_QUIET_NAN__ 1
 #define __FLT_MANT_DIG__ 24
 #define __FLT_MAX_10_EXP__ 38
-#define __FLT_MAX__ 3.40282346638528859811704183484516925e+38F
+#define __FLT_MAX__ 3.40282347e+38F
 #define __FLT_MAX_EXP__ 128
 #define __FLT_MIN_10_EXP__ (-37)
-#define __FLT_MIN__ 1.17549435082228750796873653722224568e-38F
+#define __FLT_MIN__ 1.17549435e-38F
 #define __FLT_MIN_EXP__ (-125)
 #define __FLT_RADIX__ 2
 #define FOPEN_MAX 16
@@ -217,20 +134,16 @@
 #define __GCC_ATOMIC_SHORT_LOCK_FREE 2
 #define __GCC_ATOMIC_TEST_AND_SET_TRUEVAL 1
 #define __GCC_ATOMIC_WCHAR_T_LOCK_FREE 2
-#define __GCC_HAVE_DWARF2_CFI_ASM 1
 #define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 1
 #define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 1
 #define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 1
 #define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 1
-#define __GCC_IEC_559 2
-#define __GCC_IEC_559_COMPLEX 2
-#define _GCC_SIZE_T 
 #define __getc_unlocked_body(_fp) (__glibc_unlikely ((_fp)->_IO_read_ptr >= (_fp)->_IO_read_end) ? __uflow (_fp) : *(unsigned char *) (_fp)->_IO_read_ptr++)
 #define __GID_T_TYPE __U32_TYPE
 #define __GLIBC__ 2
 #define __glibc_c99_flexarr_available 1
-#define __glibc_clang_has_extension(ext) 0
-#define __glibc_clang_prereq(maj,min) 0
+#define __glibc_clang_has_extension(ext) __has_extension (ext)
+#define __glibc_clang_prereq(maj,min) ((__clang_major__ << 16) + __clang_minor__ >= ((maj) << 16) + (min))
 #define __glibc_has_attribute(attr) __has_attribute (attr)
 #define __glibc_likely(cond) __builtin_expect ((cond), 1)
 #define __glibc_macro_warning1(message) _Pragma (#message)
@@ -248,70 +161,85 @@
 #define __GLIBC_USE_IEC_60559_TYPES_EXT 0
 #define __GLIBC_USE_ISOC2X 0
 #define __GLIBC_USE_LIB_EXT2 0
-#define __GNUC__ 9
-#define __GNUC_MINOR__ 3
-#define __GNUC_PATCHLEVEL__ 0
+#define __GNUC__ 4
+#define __GNUC_MINOR__ 2
+#define __GNUC_PATCHLEVEL__ 1
 #define __GNUC_PREREQ(maj,min) ((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))
 #define __GNUC_STDC_INLINE__ 1
-#define __GNUC_VA_LIST 
+#define __GNUC_VA_LIST 1
 #define __GNU_LIBRARY__ 6
 #define __gnu_linux__ 1
-#define __GXX_ABI_VERSION 1013
-#define __has_include_next(STR) __has_include_next__(STR)
-#define __has_include(STR) __has_include__(STR)
+#define __GXX_ABI_VERSION 1002
 #define __HAVE_GENERIC_SELECTION 1
-#define __HAVE_SPECULATION_SAFE_VALUE 1
 #define HELLO_STRING "Hello, Linux!"
 #define __ID_T_TYPE __U32_TYPE
 #define __INO64_T_TYPE __UQUAD_TYPE
 #define __INO_T_MATCHES_INO64_T 1
 #define __INO_T_TYPE __SYSCALL_ULONG_TYPE
-#define __INT16_C(c) c
-#define __INT16_MAX__ 0x7fff
-#define __INT16_TYPE__ short int
-#define __INT32_C(c) c
-#define __INT32_MAX__ 0x7fffffff
+#define __INT16_C_SUFFIX__ 
+#define __INT16_FMTd__ "hd"
+#define __INT16_FMTi__ "hi"
+#define __INT16_MAX__ 32767
+#define __INT16_TYPE__ short
+#define __INT32_C_SUFFIX__ 
+#define __INT32_FMTd__ "d"
+#define __INT32_FMTi__ "i"
+#define __INT32_MAX__ 2147483647
 #define __INT32_TYPE__ int
-#define __INT64_C(c) c ## L
-#define __INT64_MAX__ 0x7fffffffffffffffL
+#define __INT64_C_SUFFIX__ L
+#define __INT64_FMTd__ "ld"
+#define __INT64_FMTi__ "li"
+#define __INT64_MAX__ 9223372036854775807L
 #define __INT64_TYPE__ long int
-#define __INT8_C(c) c
-#define __INT8_MAX__ 0x7f
+#define __INT8_C_SUFFIX__ 
+#define __INT8_FMTd__ "hhd"
+#define __INT8_FMTi__ "hhi"
+#define __INT8_MAX__ 127
 #define __INT8_TYPE__ signed char
-#define __INT_FAST16_MAX__ 0x7fffffffffffffffL
-#define __INT_FAST16_TYPE__ long int
-#define __INT_FAST16_WIDTH__ 64
-#define __INT_FAST32_MAX__ 0x7fffffffffffffffL
-#define __INT_FAST32_TYPE__ long int
-#define __INT_FAST32_WIDTH__ 64
-#define __INT_FAST64_MAX__ 0x7fffffffffffffffL
+#define __INT_FAST16_FMTd__ "hd"
+#define __INT_FAST16_FMTi__ "hi"
+#define __INT_FAST16_MAX__ 32767
+#define __INT_FAST16_TYPE__ short
+#define __INT_FAST32_FMTd__ "d"
+#define __INT_FAST32_FMTi__ "i"
+#define __INT_FAST32_MAX__ 2147483647
+#define __INT_FAST32_TYPE__ int
+#define __INT_FAST64_FMTd__ "ld"
+#define __INT_FAST64_FMTi__ "li"
+#define __INT_FAST64_MAX__ 9223372036854775807L
 #define __INT_FAST64_TYPE__ long int
-#define __INT_FAST64_WIDTH__ 64
-#define __INT_FAST8_MAX__ 0x7f
+#define __INT_FAST8_FMTd__ "hhd"
+#define __INT_FAST8_FMTi__ "hhi"
+#define __INT_FAST8_MAX__ 127
 #define __INT_FAST8_TYPE__ signed char
-#define __INT_FAST8_WIDTH__ 8
-#define __INT_LEAST16_MAX__ 0x7fff
-#define __INT_LEAST16_TYPE__ short int
-#define __INT_LEAST16_WIDTH__ 16
-#define __INT_LEAST32_MAX__ 0x7fffffff
+#define __INT_LEAST16_FMTd__ "hd"
+#define __INT_LEAST16_FMTi__ "hi"
+#define __INT_LEAST16_MAX__ 32767
+#define __INT_LEAST16_TYPE__ short
+#define __INT_LEAST32_FMTd__ "d"
+#define __INT_LEAST32_FMTi__ "i"
+#define __INT_LEAST32_MAX__ 2147483647
 #define __INT_LEAST32_TYPE__ int
-#define __INT_LEAST32_WIDTH__ 32
-#define __INT_LEAST64_MAX__ 0x7fffffffffffffffL
+#define __INT_LEAST64_FMTd__ "ld"
+#define __INT_LEAST64_FMTi__ "li"
+#define __INT_LEAST64_MAX__ 9223372036854775807L
 #define __INT_LEAST64_TYPE__ long int
-#define __INT_LEAST64_WIDTH__ 64
-#define __INT_LEAST8_MAX__ 0x7f
+#define __INT_LEAST8_FMTd__ "hhd"
+#define __INT_LEAST8_FMTi__ "hhi"
+#define __INT_LEAST8_MAX__ 127
 #define __INT_LEAST8_TYPE__ signed char
-#define __INT_LEAST8_WIDTH__ 8
-#define __INT_MAX__ 0x7fffffff
-#define __INTMAX_C(c) c ## L
-#define __INTMAX_MAX__ 0x7fffffffffffffffL
+#define __INT_MAX__ 2147483647
+#define __INTMAX_C_SUFFIX__ L
+#define __INTMAX_FMTd__ "ld"
+#define __INTMAX_FMTi__ "li"
+#define __INTMAX_MAX__ 9223372036854775807L
 #define __INTMAX_TYPE__ long int
 #define __INTMAX_WIDTH__ 64
-#define __INTPTR_MAX__ 0x7fffffffffffffffL
+#define __INTPTR_FMTd__ "ld"
+#define __INTPTR_FMTi__ "li"
+#define __INTPTR_MAX__ 9223372036854775807L
 #define __INTPTR_TYPE__ long int
 #define __INTPTR_WIDTH__ 64
-#define ___int_size_t_h 
-#define __INT_WIDTH__ 32
 #define _IO_EOF_SEEN 0x0010
 #define _IO_ERR_SEEN 0x0020
 #define _IOFBF 0
@@ -324,65 +252,72 @@
 #define __KEY_T_TYPE __S32_TYPE
 #define L_ctermid 9
 #define __LDBL_DECIMAL_DIG__ 21
-#define __LDBL_DENORM_MIN__ 3.64519953188247460252840593361941982e-4951L
+#define __LDBL_DENORM_MIN__ 3.64519953188247460253e-4951L
 #define __LDBL_DIG__ 18
-#define __LDBL_EPSILON__ 1.08420217248550443400745280086994171e-19L
+#define __LDBL_EPSILON__ 1.08420217248550443401e-19L
 #define __LDBL_HAS_DENORM__ 1
 #define __LDBL_HAS_INFINITY__ 1
 #define __LDBL_HAS_QUIET_NAN__ 1
 #define __LDBL_MANT_DIG__ 64
 #define __LDBL_MAX_10_EXP__ 4932
-#define __LDBL_MAX__ 1.18973149535723176502126385303097021e+4932L
+#define __LDBL_MAX__ 1.18973149535723176502e+4932L
 #define __LDBL_MAX_EXP__ 16384
 #define __LDBL_MIN_10_EXP__ (-4931)
-#define __LDBL_MIN__ 3.36210314311209350626267781732175260e-4932L
+#define __LDBL_MIN__ 3.36210314311209350626e-4932L
 #define __LDBL_MIN_EXP__ (-16381)
 #define __LDBL_REDIR1(name,proto,alias) name proto
 #define __LDBL_REDIR1_NTH(name,proto,alias) name proto __THROW
 #define __LDBL_REDIR_DECL(name) 
 #define __LDBL_REDIR(name,proto) name proto
 #define __LDBL_REDIR_NTH(name,proto) name proto __THROW
-#define __LEAF_ATTR __attribute__ ((__leaf__))
-#define __LEAF , __leaf__
+#define __LEAF 
+#define __LEAF_ATTR 
 #define __linux 1
 #define __linux__ 1
 #define linux 1
+#define __LITTLE_ENDIAN__ 1
+#define __llvm__ 1
 #define __LONG_DOUBLE_USES_FLOAT128 0
-#define __LONG_LONG_MAX__ 0x7fffffffffffffffLL
-#define __LONG_LONG_WIDTH__ 64
-#define __LONG_MAX__ 0x7fffffffffffffffL
-#define __LONG_WIDTH__ 64
+#define __LONG_LONG_MAX__ 9223372036854775807LL
+#define __LONG_MAX__ 9223372036854775807L
 #define __LP64__ 1
 #define _LP64 1
 #define L_tmpnam 20
 #define ____mbstate_t_defined 1
 #define __MMX__ 1
 #define __MODE_T_TYPE __U32_TYPE
+#define __need___va_list 
 #define __NLINK_T_TYPE __SYSCALL_ULONG_TYPE
 #define __NO_INLINE__ 1
+#define __NO_MATH_INLINES 1
 #define __nonnull(params) __attribute__ ((__nonnull__ params))
 #define __NTH(fct) __attribute__ ((__nothrow__ __LEAF)) fct
 #define __NTHNL(fct) __attribute__ ((__nothrow__)) fct
-#define NULL ((void *)0)
+#define NULL ((void*)0)
+#define __OBJC_BOOL_IS_BOOL 0
 #define __OFF64_T_TYPE __SQUAD_TYPE
 #define __off_t_defined 
 #define __OFF_T_MATCHES_OFF64_T 1
 #define __OFF_T_TYPE __SYSCALL_SLONG_TYPE
+#define __OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES 3
+#define __OPENCL_MEMORY_SCOPE_DEVICE 2
+#define __OPENCL_MEMORY_SCOPE_SUB_GROUP 4
+#define __OPENCL_MEMORY_SCOPE_WORK_GROUP 1
+#define __OPENCL_MEMORY_SCOPE_WORK_ITEM 0
 #define __ORDER_BIG_ENDIAN__ 4321
 #define __ORDER_LITTLE_ENDIAN__ 1234
 #define __ORDER_PDP_ENDIAN__ 3412
 #define __P(args) args
-#define __pic__ 2
-#define __PIC__ 2
 #define __PID_T_TYPE __S32_TYPE
-#define __pie__ 2
-#define __PIE__ 2
 #define __PMT(args) args
+#define __POINTER_WIDTH__ 64
 #define _POSIX_C_SOURCE 200809L
 #define _POSIX_SOURCE 1
 #define __PRAGMA_REDEFINE_EXTNAME 1
 #define P_tmpdir "/tmp"
-#define __PTRDIFF_MAX__ 0x7fffffffffffffffL
+#define __PTRDIFF_FMTd__ "ld"
+#define __PTRDIFF_FMTi__ "li"
+#define __PTRDIFF_MAX__ 9223372036854775807L
 #define __PTRDIFF_TYPE__ long int
 #define __PTRDIFF_WIDTH__ 64
 #define __ptr_t void *
@@ -400,24 +335,25 @@
 #define __S16_TYPE short int
 #define __S32_TYPE int
 #define __S64_TYPE long int
-#define __SCHAR_MAX__ 0x7f
-#define __SCHAR_WIDTH__ 8
+#define __SCHAR_MAX__ 127
 #define SEEK_CUR 1
 #define SEEK_END 2
 #define SEEK_SET 0
 #define __SEG_FS 1
+#define __seg_fs __attribute__((address_space(257)))
 #define __SEG_GS 1
-#define __SHRT_MAX__ 0x7fff
-#define __SHRT_WIDTH__ 16
-#define __SIG_ATOMIC_MAX__ 0x7fffffff
-#define __SIG_ATOMIC_MIN__ (-__SIG_ATOMIC_MAX__ - 1)
-#define __SIG_ATOMIC_TYPE__ int
+#define __seg_gs __attribute__((address_space(256)))
+#define __SHRT_MAX__ 32767
+#define __SIG_ATOMIC_MAX__ 2147483647
 #define __SIG_ATOMIC_WIDTH__ 32
-#define __SIZE_MAX__ 0xffffffffffffffffUL
+#define __SIZE_FMTo__ "lo"
+#define __SIZE_FMTu__ "lu"
+#define __SIZE_FMTx__ "lx"
+#define __SIZE_FMTX__ "lX"
+#define __SIZE_MAX__ 18446744073709551615UL
 #define __SIZEOF_DOUBLE__ 8
 #define __SIZEOF_FLOAT128__ 16
 #define __SIZEOF_FLOAT__ 4
-#define __SIZEOF_FLOAT80__ 16
 #define __SIZEOF_INT128__ 16
 #define __SIZEOF_INT__ 4
 #define __SIZEOF_LONG__ 8
@@ -429,16 +365,7 @@
 #define __SIZEOF_SIZE_T__ 8
 #define __SIZEOF_WCHAR_T__ 4
 #define __SIZEOF_WINT_T__ 4
-#define __size_t 
-#define __size_t__ 
-#define __SIZE_T 
-#define __SIZE_T__ 
 #define _SIZE_T 
-#define _SIZE_T_ 
-#define _SIZET_ 
-#define _SIZE_T_DECLARED 
-#define _SIZE_T_DEFINED 
-#define _SIZE_T_DEFINED_ 
 #define __SIZE_TYPE__ long unsigned int
 #define __SIZE_WIDTH__ 64
 #define __SLONG32_TYPE int
@@ -450,8 +377,8 @@
 #define __SSE_MATH__ 1
 #define __ssize_t_defined 
 #define __SSIZE_T_TYPE __SWORD_TYPE
-#define __SSP_STRONG__ 3
 #define __STATFS_MATCHES_STATFS64 1
+#define __STDARG_H 
 #define __STDC__ 1
 #define __STDC_HOSTED__ 1
 #define __STDC_IEC_559__ 1
@@ -460,7 +387,7 @@
 #define _STDC_PREDEF_H 1
 #define __STDC_UTF_16__ 1
 #define __STDC_UTF_32__ 1
-#define __STDC_VERSION__ 201710L
+#define __STDC_VERSION__ 201112L
 #define stderr stderr
 #define stdin stdin
 #define _STDIO_H 1
@@ -483,7 +410,6 @@
 #define __SYSCALL_ULONG_TYPE __ULONGWORD_TYPE
 #define __SYSCALL_WORDSIZE 64
 #define _SYS_CDEFS_H 1
-#define _SYS_SIZE_T_H 
 #define __THROW __attribute__ ((__nothrow__ __LEAF))
 #define __THROWNL __attribute__ ((__nothrow__))
 #define __TIME64_T_TYPE __TIME_T_TYPE
@@ -491,45 +417,102 @@
 #define __TIMESIZE __WORDSIZE
 #define __TIME_T_TYPE __SYSCALL_SLONG_TYPE
 #define TMP_MAX 238328
-#define _T_SIZE 
-#define _T_SIZE_ 
+#define __tune_k8__ 1
 #define __U16_TYPE unsigned short int
 #define __U32_TYPE unsigned int
 #define __U64_TYPE unsigned long int
 #define __UID_T_TYPE __U32_TYPE
-#define __UINT16_C(c) c
-#define __UINT16_MAX__ 0xffff
-#define __UINT16_TYPE__ short unsigned int
-#define __UINT32_C(c) c ## U
-#define __UINT32_MAX__ 0xffffffffU
+#define __UINT16_C_SUFFIX__ 
+#define __UINT16_FMTo__ "ho"
+#define __UINT16_FMTu__ "hu"
+#define __UINT16_FMTx__ "hx"
+#define __UINT16_FMTX__ "hX"
+#define __UINT16_MAX__ 65535
+#define __UINT16_TYPE__ unsigned short
+#define __UINT32_C_SUFFIX__ U
+#define __UINT32_FMTo__ "o"
+#define __UINT32_FMTu__ "u"
+#define __UINT32_FMTx__ "x"
+#define __UINT32_FMTX__ "X"
+#define __UINT32_MAX__ 4294967295U
 #define __UINT32_TYPE__ unsigned int
-#define __UINT64_C(c) c ## UL
-#define __UINT64_MAX__ 0xffffffffffffffffUL
+#define __UINT64_C_SUFFIX__ UL
+#define __UINT64_FMTo__ "lo"
+#define __UINT64_FMTu__ "lu"
+#define __UINT64_FMTx__ "lx"
+#define __UINT64_FMTX__ "lX"
+#define __UINT64_MAX__ 18446744073709551615UL
 #define __UINT64_TYPE__ long unsigned int
-#define __UINT8_C(c) c
-#define __UINT8_MAX__ 0xff
+#define __UINT8_C_SUFFIX__ 
+#define __UINT8_FMTo__ "hho"
+#define __UINT8_FMTu__ "hhu"
+#define __UINT8_FMTx__ "hhx"
+#define __UINT8_FMTX__ "hhX"
+#define __UINT8_MAX__ 255
 #define __UINT8_TYPE__ unsigned char
-#define __UINT_FAST16_MAX__ 0xffffffffffffffffUL
-#define __UINT_FAST16_TYPE__ long unsigned int
-#define __UINT_FAST32_MAX__ 0xffffffffffffffffUL
-#define __UINT_FAST32_TYPE__ long unsigned int
-#define __UINT_FAST64_MAX__ 0xffffffffffffffffUL
+#define __UINT_FAST16_FMTo__ "ho"
+#define __UINT_FAST16_FMTu__ "hu"
+#define __UINT_FAST16_FMTx__ "hx"
+#define __UINT_FAST16_FMTX__ "hX"
+#define __UINT_FAST16_MAX__ 65535
+#define __UINT_FAST16_TYPE__ unsigned short
+#define __UINT_FAST32_FMTo__ "o"
+#define __UINT_FAST32_FMTu__ "u"
+#define __UINT_FAST32_FMTx__ "x"
+#define __UINT_FAST32_FMTX__ "X"
+#define __UINT_FAST32_MAX__ 4294967295U
+#define __UINT_FAST32_TYPE__ unsigned int
+#define __UINT_FAST64_FMTo__ "lo"
+#define __UINT_FAST64_FMTu__ "lu"
+#define __UINT_FAST64_FMTx__ "lx"
+#define __UINT_FAST64_FMTX__ "lX"
+#define __UINT_FAST64_MAX__ 18446744073709551615UL
 #define __UINT_FAST64_TYPE__ long unsigned int
-#define __UINT_FAST8_MAX__ 0xff
+#define __UINT_FAST8_FMTo__ "hho"
+#define __UINT_FAST8_FMTu__ "hhu"
+#define __UINT_FAST8_FMTx__ "hhx"
+#define __UINT_FAST8_FMTX__ "hhX"
+#define __UINT_FAST8_MAX__ 255
 #define __UINT_FAST8_TYPE__ unsigned char
-#define __UINT_LEAST16_MAX__ 0xffff
-#define __UINT_LEAST16_TYPE__ short unsigned int
-#define __UINT_LEAST32_MAX__ 0xffffffffU
+#define __UINT_LEAST16_FMTo__ "ho"
+#define __UINT_LEAST16_FMTu__ "hu"
+#define __UINT_LEAST16_FMTx__ "hx"
+#define __UINT_LEAST16_FMTX__ "hX"
+#define __UINT_LEAST16_MAX__ 65535
+#define __UINT_LEAST16_TYPE__ unsigned short
+#define __UINT_LEAST32_FMTo__ "o"
+#define __UINT_LEAST32_FMTu__ "u"
+#define __UINT_LEAST32_FMTx__ "x"
+#define __UINT_LEAST32_FMTX__ "X"
+#define __UINT_LEAST32_MAX__ 4294967295U
 #define __UINT_LEAST32_TYPE__ unsigned int
-#define __UINT_LEAST64_MAX__ 0xffffffffffffffffUL
+#define __UINT_LEAST64_FMTo__ "lo"
+#define __UINT_LEAST64_FMTu__ "lu"
+#define __UINT_LEAST64_FMTx__ "lx"
+#define __UINT_LEAST64_FMTX__ "lX"
+#define __UINT_LEAST64_MAX__ 18446744073709551615UL
 #define __UINT_LEAST64_TYPE__ long unsigned int
-#define __UINT_LEAST8_MAX__ 0xff
+#define __UINT_LEAST8_FMTo__ "hho"
+#define __UINT_LEAST8_FMTu__ "hhu"
+#define __UINT_LEAST8_FMTx__ "hhx"
+#define __UINT_LEAST8_FMTX__ "hhX"
+#define __UINT_LEAST8_MAX__ 255
 #define __UINT_LEAST8_TYPE__ unsigned char
-#define __UINTMAX_C(c) c ## UL
-#define __UINTMAX_MAX__ 0xffffffffffffffffUL
+#define __UINTMAX_C_SUFFIX__ UL
+#define __UINTMAX_FMTo__ "lo"
+#define __UINTMAX_FMTu__ "lu"
+#define __UINTMAX_FMTx__ "lx"
+#define __UINTMAX_FMTX__ "lX"
+#define __UINTMAX_MAX__ 18446744073709551615UL
 #define __UINTMAX_TYPE__ long unsigned int
-#define __UINTPTR_MAX__ 0xffffffffffffffffUL
+#define __UINTMAX_WIDTH__ 64
+#define __UINTPTR_FMTo__ "lo"
+#define __UINTPTR_FMTu__ "lu"
+#define __UINTPTR_FMTx__ "lx"
+#define __UINTPTR_FMTX__ "lX"
+#define __UINTPTR_MAX__ 18446744073709551615UL
 #define __UINTPTR_TYPE__ long unsigned int
+#define __UINTPTR_WIDTH__ 64
 #define __ULONG32_TYPE unsigned int
 #define __ULONGWORD_TYPE unsigned long int
 #define __unix 1
@@ -552,19 +535,22 @@
 #define __USE_XOPEN2K 1
 #define __USE_XOPEN2K8 1
 #define __UWORD_TYPE unsigned long int
-#define __va_arg_pack() __builtin_va_arg_pack ()
-#define __va_arg_pack_len() __builtin_va_arg_pack_len ()
+#define va_arg(ap,type) __builtin_va_arg(ap, type)
+#define va_copy(dest,src) __builtin_va_copy(dest, src)
+#define __va_copy(d,s) __builtin_va_copy(d,s)
+#define va_end(ap) __builtin_va_end(ap)
+#define _VA_LIST 
 #define _VA_LIST_DEFINED 
-#define __VERSION__ "9.3.0"
-#define __warnattr(msg) __attribute__((__warning__ (msg)))
-#define __warndecl(name,msg) extern void name (void) __attribute__((__warning__ (msg)))
-#define __WCHAR_MAX__ 0x7fffffff
-#define __WCHAR_MIN__ (-__WCHAR_MAX__ - 1)
+#define va_start(ap,param) __builtin_va_start(ap, param)
+#define __VERSION__ "Clang 10.0.0 "
+#define __warnattr(msg) 
+#define __warndecl(name,msg) extern void name (void)
+#define __WCHAR_MAX__ 2147483647
 #define __WCHAR_TYPE__ int
 #define __WCHAR_WIDTH__ 32
-#define __WINT_MAX__ 0xffffffffU
-#define __WINT_MIN__ 0U
+#define __WINT_MAX__ 4294967295U
 #define __WINT_TYPE__ unsigned int
+#define __WINT_UNSIGNED__ 1
 #define __WINT_WIDTH__ 32
 #define __WORDSIZE 64
 #define __WORDSIZE_TIME64_COMPAT32 1
