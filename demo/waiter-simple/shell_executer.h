/**
 * @file shell_executer.h
 * @brief シェルスクリプト実行モジュールの定義
 * @sa shell_executer.c
 */

/// exec_shellの戻り値の定義一覧
typedef enum {
	shell_success,		///< 実行が正常に終了し、戻り値が0であった。
	shell_failure,		///< 戻り値が0以外であった。
	shell_killed,		///< スクリプトがシグナルにより終了した。
	shell_not_found,	///< スクリプトが存在しなかった。
	systemcall_error,	///< システムコールが失敗した。
} shell_executer_error;

/// シェルインタプリタ
#define SHELL_INTERPRETER "/bin/sh"

/**
 * @brief 任意のシェルを実行するプログラム。
 * @param shell_path    シェルスクリプトのパス
 * @return shell_executer_error 型のエラー
**/
shell_executer_error exec_shell(const char* shell_path);
