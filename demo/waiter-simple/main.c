/**
 * @file main.c
 * 
 * @brief スクリプト実行の待機プログラムのサンプル。
 *  単純性のため、 setitimer を使用している。
 *  高度なタイマのためには timer_create() を使用すること。
**/
#include <stdio.h>
#include <stdlib.h>     // EXIT_SUCCESS
#include <unistd.h>		// getcwd
#include <linux/limits.h> // PATH_MAX
#include <string.h>		// strcat
#include <sys/time.h>	// setitimer
#include <signal.h>		// signal
#include <sys/types.h>	// waitpid
#include <sys/wait.h>	// waitpid

#include "shell_executer.h"


#define MY_SHELL "sleep.sh"

void alarm_handler(int signal_no);


/***
 * @brief スクリプト実行プログラムのサンプル。
 * @param なし。
 * @retval 0 実行が成功した。
 * @retval 1 実行が失敗、または中断や異常終了した。
*/
int main(void)
{
	shell_executer_error retval = 0;
	char *buf = NULL;
	struct itimerval timer_interval;
	pid_t pid_of_child_process;
	int w, wstatus;

	buf = getcwd(NULL, PATH_MAX);
	if(buf == NULL)
	{
		perror("getcwd");
	}
	printf("cwd=%s\n", buf);
	strncat(buf, "/", PATH_MAX);
	strncat(buf, MY_SHELL, PATH_MAX);
	printf("shell=%s\n", buf);

	pid_of_child_process = fork();

	if( pid_of_child_process == -1)
	{
		return EXIT_FAILURE;
	}
	else if( pid_of_child_process == 0)
	{ /* Code executed by child */
		w = execl(SHELL_INTERPRETER, SHELL_INTERPRETER, "-c", buf, NULL);
		if(w == -1)
		{
			return (shell_executer_error)systemcall_error;
		}
		// not reach
	}
	/* Code executed by parent */
	// ハンドラの登録
	signal(SIGALRM, alarm_handler);

	printf("child is %d\n", pid_of_child_process);
	
	// インターバルの設定
	memset(&timer_interval, 0, sizeof(struct itimerval));
	timer_interval.it_interval.tv_sec = 1;
	timer_interval.it_value.tv_sec = 1;

	// タイマーのセット
	retval = setitimer(ITIMER_REAL, &timer_interval, NULL);
	if(retval != 0)
	{
		perror("setitimer");
		return EXIT_FAILURE;
	}

	// child の終了を待機
	w = waitpid(pid_of_child_process, &wstatus, 0);
	if (w == -1)
	{
		printf("systemcall_error\n");
		// return (shell_executer_error)systemcall_error;
		return EXIT_FAILURE;
	}
	if (WIFEXITED(wstatus))
	{
		if(WEXITSTATUS(wstatus) == 127)
		{
			printf("shell_not_found\n");
			// return (shell_executer_error)shell_not_found;
			return EXIT_FAILURE;
		}
		else if(WEXITSTATUS(wstatus) != EXIT_SUCCESS)
		{
			printf("shell_failure\n");
			// return (shell_executer_error)shell_failure;
			return EXIT_FAILURE;
		}
	}
	else if (WIFSIGNALED(wstatus))
	{
		printf("shell_killed\n");
		// return (shell_executer_error)shell_killed;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

void alarm_handler(int signal_no)
{
	printf("waiting...\n");
}
