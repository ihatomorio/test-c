#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#define PRINT_SIZE( type ) printf( #type ":\t%zu\n", sizeof( type ))

int main(int argc, char const *argv[])
{
    PRINT_SIZE( char );
    PRINT_SIZE( short );
    PRINT_SIZE( int );
    PRINT_SIZE( long );
    printf("\n");

    PRINT_SIZE( int8_t );
    PRINT_SIZE( int16_t );
    PRINT_SIZE( int32_t );
    PRINT_SIZE( int64_t );
    printf("\n");

    PRINT_SIZE( int_fast8_t );
    PRINT_SIZE( int_fast16_t );
    PRINT_SIZE( int_fast32_t );
    PRINT_SIZE( int_fast64_t );
    printf("\n");

    PRINT_SIZE( int_least8_t );
    PRINT_SIZE( int_least16_t );
    PRINT_SIZE( int_least32_t );
    PRINT_SIZE( int_least64_t );

    return 0;
}
