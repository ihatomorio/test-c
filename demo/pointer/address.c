#include <stdio.h>  //printf()
#include <stdlib.h> //calloc()

// writable only value
void pointer_writable_func(void ** const ptr, size_t len)
{
    printf("*ptr: %p\n", *ptr);
    *ptr = calloc(len, sizeof(int));
    // **ptr = 5; //invalid
    printf("*ptr: %p\n", *ptr);
}

// writable only address
void writable_func(const int* num){
    printf("writable_func: %p\n", num);
    num = NULL;
    // *num = 5; // invalid
    printf("writable_func: %p\n", num); //OK
}

// writable only value
void writable_func2(int* const num){
    // num = NULL; // invalid
    *num = 5;
    printf("writable_func2: %d\n", *num);
}

void read_only_func(const int* const num){
    // num = NULL; // invalid
    // *num = 5; // invalid
    printf("read_only_func: %d\n", *num);
}

void read_only_string( const char* const string){
    // string[0] = 'b'; //invalid
    // &string = NULL; //invalid
    // *string = NULL; //invalid
    printf("%s\n", string);
}

int main(int argc, char const *argv[])
{
    int a = 5;
    int *b = NULL;
    int *c = NULL;

    printf(" a: %d\n", a);
    printf("&a: %p\n", &a);
    printf("\n");

    b = (int *)calloc(4, sizeof(int));
    printf("*b: %d\n",*b);
    printf(" b: %p\n", b);
    printf("&b: %p\n", &b);
    printf("\n");

    pointer_writable_func((void *)&c, 4);
    printf("c0: %d\n",c[3]);
    printf(" c: %p\n", c);
    printf("&c: %p\n", &c);
    printf("\n");

    int d = 0;
    printf(" d: %d\n", d);
    printf("&d: %p\n",&d);
    writable_func(&d);
    printf(" d: %d\n", d);
    printf("&d: %p\n",&d);
    printf("\n");

    int e = 0;
    printf(" e: %d\n", e);
    printf("&e: %p\n",&e);
    writable_func2(&e);
    printf(" e: %d\n", e);
    printf("&e: %p\n",&e);
    printf("\n");

    char f[128] = "hogehoge";
    read_only_string(f);

    return 0;
}
