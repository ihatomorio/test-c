/**
 * @file shell_executer.c
 * 
 * @brief shell_executer.h の実装ファイル
**/

#include <unistd.h>     // fork
#include <sys/types.h>  // wait
#include <sys/wait.h>   // wait
#include <stdlib.h>     // EXIT_SUCCESS

#include "shell_executer.h"


shell_executer_error exec_shell(const char* shell_path)
{
	pid_t pid_of_child_process, w;
	int wstatus;
	int ret = -1;

	pid_of_child_process = fork();
	if (pid_of_child_process == -1)
	{
		return (shell_executer_error)systemcall_error;
	}
	else if (pid_of_child_process == 0)
	{ /* Code executed by child */
		ret = execl(SHELL_INTERPRETER, SHELL_INTERPRETER, "-c", shell_path, NULL);
		if(ret == -1)
		{
			return (shell_executer_error)systemcall_error;
		}
		// not reach
	}
	/* Code executed by parent */
	w = waitpid(pid_of_child_process, &wstatus, 0);
	if (w == -1)
	{
		return (shell_executer_error)systemcall_error;
	}

	if (WIFEXITED(wstatus))
	{
		if(WEXITSTATUS(wstatus) == 127)
		{
			return (shell_executer_error)shell_not_found;
		}
		else if(WEXITSTATUS(wstatus) != EXIT_SUCCESS)
		{
			return (shell_executer_error)shell_failure;
		}
	}
	else if (WIFSIGNALED(wstatus))
	{
		return (shell_executer_error)shell_killed;
	}

	return (shell_executer_error)shell_success;
}
