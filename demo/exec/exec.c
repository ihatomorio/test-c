/**
 * @file exec.c
 * 
 * @brief スクリプト実行プログラムのサンプル。
**/
#include <stdio.h>
#include <stdlib.h>

#include "shell_executer.h"


#define MY_SHELL "/home/ihatomorio/repos/test-c/demo/exec/sleep.sh"

/***
 * @brief スクリプト実行プログラムのサンプル。
 * @param なし。
 * @retval 0 実行が成功した。
 * @retval 1 実行が失敗、または中断や異常終了した。
*/
int main(void)
{
	shell_executer_error retval = 0;
	retval = exec_shell(MY_SHELL);
	if(retval != 0)
	{
		printf("fail with %d\n", (int)retval);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
