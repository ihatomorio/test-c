#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

int get_machine_interface_list(struct ifconf *interface_list);

int main(int argc, char const *argv[])
{
    struct ifconf interface_list = {0};
    int i = 0;

    int interfaces_at_machine = get_machine_interface_list(&interface_list);
    if (interfaces_at_machine == -1)
    {
        return 1;
    }

    for (i = 0; i < interfaces_at_machine; i++)
    {
        printf("%d: %s\n", i + 1, (interface_list.ifc_req[i].ifr_name));
    }

    return 0;
}

int get_machine_interface_list(struct ifconf *interface_list)
{
    int socket_descripter = -1;
    int interfaces_at_machine = 0;
    int tmp_return = -1;

    if( interface_list == NULL)
    {
        return -1;
    }

    interface_list->ifc_len = 0;

    socket_descripter = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_descripter == -1)
    {
        perror(NULL);
        return -1;
    }

    tmp_return = ioctl(socket_descripter, SIOCGIFCONF, interface_list);
    if (tmp_return == -1)
    {
        perror(NULL);
        return -1;
    }

    interface_list->ifc_buf = (char *)calloc(interface_list->ifc_len, sizeof(char));

    if(interface_list->ifc_buf == NULL)
    {
        perror(NULL);
        return -1;
    }

    tmp_return = ioctl(socket_descripter, SIOCGIFCONF, interface_list);
    if (tmp_return == -1)
    {
        perror(NULL);
        return -1;
    }

    interfaces_at_machine = interface_list->ifc_len / sizeof(struct ifreq);

    return interfaces_at_machine;
}