.PHONY: all docs
all:
	make -C CUnit
	make -C demo
	# make -C library
	make -C mysendudp
	make -C rawsock

docs:
	doxygen

.PHONY: clean
clean:
	make clean -C CUnit
	make clean -C demo
	# make clean -C library
	# make clean -C mysendudp
	# make clean -C rawsock

.PHONY: test
test:
	make test -C CUnit
	make test -C demo
	# make test -C library
	# make test -C mysendudp
	# make test -C rawsock
