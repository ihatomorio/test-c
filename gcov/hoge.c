#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    pid_t pid = -1;
    pid = fork();
    int i=1;
    while(i) {
        sleep(1);
    }
    if(pid == -1)
    {
        //error
        printf("fork error!\n");
        goto final;
    }

    if(pid == 0)
    {
        //child
        printf("I am child!\n");
        sleep(1);
        alarm(5);

        //sigalm
    }
    else
    {
        //parent
        printf("I am parent!\n");
    }

final:
    return 0;
}
