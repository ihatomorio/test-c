// original send icmp program

#include <errno.h>           //errno
#include <stddef.h>          //size_t
#include <stdio.h>           //printf()
#include <stdlib.h>          //calloc(), free()
#include <string.h>          //memcpy()
#include <unistd.h>          //close()
#include <inttypes.h>        //PRIx16
#include <arpa/inet.h>       //inet_aton()
#include <sys/types.h>       //socket(), send()
#include <sys/socket.h>      //socket(), send(), inet_aton()
#include <sys/ioctl.h>       //ioctl()
#include <netinet/in.h>      //inet_aton()
#include <netinet/ip.h>      //iphdr
#include <netinet/ip_icmp.h> //icmphdr
#include <net/ethernet.h>
#include <netpacket/packet.h> //sockaddr_ll
#include <linux/if.h>         //ifreq

uint16_t checksum(uint16_t *buf, size_t buflen);
int make_icmp_echo_message_packet(uint16_t id, uint16_t seq, void **buf, size_t *buflen);
int make_ip_packet(uint16_t id, uint8_t protocol, const char *src_ip, const char *dst_ip, void **ip_packet, void *data_buf, size_t data_buf_len);
int make_ether_frame(void **ether_frame, size_t *ether_frame_len, void *data, size_t data_len);
void print_hex(void *buf, size_t buflen);
int get_raw_socket(const char *device_name);

int main(int argc, char const *argv[])
{
    int socket_descriptor = -1;
    int function_result = 0;
    int exit_status = 0;
    void *icmp_packet = NULL;
    size_t icmp_packet_len = 0;
    void *ip_packet = NULL;
    size_t ip_packet_len = 0;
    void *ether_frame = NULL;
    size_t ether_frame_length = 0;
    ssize_t sent_bytes = 0;

    //get socket
    socket_descriptor = get_raw_socket("eth0");

    if (socket_descriptor == -1)
    {
        exit_status = 1;
        goto final;
    }

    function_result = make_icmp_echo_message_packet(5653, 1, &icmp_packet, &icmp_packet_len);
    if (function_result != 0)
    {
        exit_status = 4;
        goto final;
    }

    printf("ICMP packet:\n");
    print_hex(icmp_packet, icmp_packet_len);

    function_result = make_ip_packet(56830, IPPROTO_ICMP, "172.24.209.22", "192.168.100.1", &ip_packet, icmp_packet, icmp_packet_len);
    if (function_result != 0)
    {
        exit_status = 5;
        goto final;
    }

    ip_packet_len = ntohs(((struct iphdr *)ip_packet)->tot_len);

    printf("ip_packet_len: %zu\n", ip_packet_len);
    print_hex(ip_packet, ip_packet_len);

    make_ether_frame(&ether_frame, &ether_frame_length, ip_packet, ip_packet_len);

    printf("ether_frame_length: %zu\n", ether_frame_length);
    print_hex(ether_frame, ether_frame_length);

    sent_bytes = send(socket_descriptor, ether_frame, ether_frame_length, 0);

    printf("sent_bytes: %zu\n", sent_bytes);
    if (sent_bytes < ether_frame_length)
    {
        exit_status = 6;
        goto final;
    }

final:
    free(icmp_packet);
    free(ip_packet);
    free(ether_frame);
    close(socket_descriptor);

    return exit_status;
}

int get_raw_socket(const char *device_name)
{
    int socket_descriptor = -1;
    struct ifreq ioctl_request;
    int syscall_returns = 0;
    struct sockaddr_ll sll;
    int tmp_errno = 0;

    socket_descriptor = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    tmp_errno = errno;

    if (socket_descriptor == -1)
    {
        printf("system call error: %s\n", strerror(tmp_errno));
        goto final;
    }

    strncpy(ioctl_request.ifr_name, device_name, sizeof(ioctl_request.ifr_name) - 1);

    syscall_returns = ioctl(socket_descriptor, SIOCGIFINDEX, &ioctl_request);
    tmp_errno = errno;

    if (syscall_returns == -1)
    {
        printf("system call error: %s\n", strerror(tmp_errno));
        close(socket_descriptor);
        socket_descriptor = -1;
        goto final;
    }

    sll.sll_family = PF_PACKET;
    sll.sll_protocol = htons(ETH_P_IP);
    sll.sll_ifindex = ioctl_request.ifr_ifindex;

    syscall_returns = bind(socket_descriptor, (struct sockaddr *)&sll, sizeof(sll));
    tmp_errno = errno;
    
    if (syscall_returns == -1)
    {
        printf("system call error: %s\n", strerror(tmp_errno));
        close(socket_descriptor);
        socket_descriptor = -1;
        goto final;
    }

final:
    return socket_descriptor;
}

uint16_t checksum(uint16_t *buf, size_t buflen)
{
    unsigned long sum = 0;
    uint16_t checksum;

    while (buflen > 1)
    {
        sum += *buf;
        buf++;
        buflen -= 2;
    }

    if (buflen == 1)
    {
        sum += *(unsigned char *)buf;
    }

    sum = (sum & 0xffff) + (sum >> 16);
    sum = (sum & 0xffff) + (sum >> 16);

    checksum = (uint16_t)~sum;

    return checksum;
}

int make_icmp_echo_message_packet(uint16_t id, uint16_t seq, void **buf, size_t *buflen)
{
    struct icmphdr *icmp_packet = NULL;
    int return_code = 0;

    *buflen = sizeof(struct icmphdr);
    *buf = calloc(sizeof(int8_t), *buflen);

    if (*buf == NULL)
    {
        return_code = -1;
        goto final;
    }

    icmp_packet = (struct icmphdr *)*buf;

    icmp_packet->type = ICMP_ECHO; //Echo Request
    icmp_packet->code = 0;
    icmp_packet->checksum = 0;
    icmp_packet->un.echo.id = htons(id);
    icmp_packet->un.echo.sequence = htons(seq);
    //no datas
    icmp_packet->checksum = checksum((uint16_t *)icmp_packet, sizeof(struct icmphdr));

final:
    return return_code;
}

int make_ip_packet(uint16_t id, uint8_t protocol, const char *src_ip, const char *dst_ip, void **ip_packet, void *data_buf, size_t data_buf_len)
{
    struct iphdr *ip_header = NULL;
    struct in_addr src_addr;
    struct in_addr dst_addr;
    int convert_result = 1;
    int return_code = 0;
    size_t ip_packet_len = sizeof(struct iphdr) + data_buf_len;

    *ip_packet = calloc(sizeof(int8_t), ip_packet_len);
    if (*ip_packet == NULL)
    {
        return_code = -1;
        goto final;
    }

    ip_header = (struct iphdr *)*ip_packet;

    convert_result = inet_aton(src_ip, &src_addr);
    if (convert_result == 0)
    {
        printf("inet_aton failed.\n");
        return_code = -2;
        goto final;
    }

    convert_result = inet_aton(dst_ip, &dst_addr);
    if (convert_result == 0)
    {
        printf("inet_aton failed.\n");
        return_code = -3;
        goto final;
    }

    ip_header->version = IPVERSION;
    ip_header->ihl = 5;
    ip_header->tos = 0;
    ip_header->tot_len = htons(ip_packet_len);
    ip_header->id = htons(id);
    ip_header->frag_off = htons(0x4000);
    ip_header->ttl = 255;
    ip_header->protocol = protocol;
    ip_header->check = 0;
    ip_header->saddr = src_addr.s_addr;
    ip_header->daddr = dst_addr.s_addr;

    memcpy(*ip_packet, ip_header, sizeof(struct iphdr));
    memcpy(*ip_packet + sizeof(struct iphdr), data_buf, data_buf_len);

    ip_header->check = checksum((uint16_t *)*ip_packet, ip_packet_len);

final:
    return return_code;
}

int make_ether_frame(void **ether_frame, size_t *ether_frame_len, void *data, size_t data_len)
{
    int return_value = 0;
    struct ether_header *ether_frame_header = NULL;
    if (data_len < ETHERMIN)
    {
        *ether_frame_len = ETH_ZLEN;
    }

    *ether_frame = calloc(sizeof(int8_t), *ether_frame_len);
    if (*ether_frame == NULL)
    {
        return_value = -1;
        goto final;
    }

    ether_frame_header = (struct ether_header *)*ether_frame;

    // 00a0 dec0 180c 0800 2720 91f3
    ether_frame_header->ether_dhost[0] = 0x00;
    ether_frame_header->ether_dhost[1] = 0xa0;
    ether_frame_header->ether_dhost[2] = 0xde;
    ether_frame_header->ether_dhost[3] = 0xc0;
    ether_frame_header->ether_dhost[4] = 0x18;
    ether_frame_header->ether_dhost[5] = 0x0c;

    ether_frame_header->ether_shost[0] = 0x00;
    ether_frame_header->ether_shost[1] = 0x15;
    ether_frame_header->ether_shost[2] = 0x5d;
    ether_frame_header->ether_shost[3] = 0xd8;
    ether_frame_header->ether_shost[4] = 0xfa;
    ether_frame_header->ether_shost[5] = 0xd1;

    ether_frame_header->ether_type = htons(ETHERTYPE_IP);

    memcpy(*ether_frame + ETHER_HDR_LEN, data, data_len);

final:
    return return_value;
}

void print_hex(void *buf, size_t buflen)
{
    uint8_t *poiner = (uint8_t *)buf;
    size_t offset = 0;

    for (offset = 0; offset < buflen; offset++)
    {
        printf("%02x", (uint8_t)*poiner);
        poiner++;

        if (offset % 16 == 15)
        {
            printf("\n");
            continue;
        }

        if (offset % 2 == 1)
        {
            printf(" ");
        }
    }

    printf("\n");
}
