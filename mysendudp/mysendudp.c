#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#define ARGUMENT_NUMBER 3

// argv[0]: me argc==1
int main(int argc, char const *argv[])
{
    int socket_fd = 0;
    struct sockaddr_in address_destination;
    int function_returned_number;
    int errno_save=0;

    // check command argument number
    if( argc != ARGUMENT_NUMBER + 1 ) {
        printf("args invalid: IP port message\n");
        return 1;
    }

    // get socket
    socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    errno_save = errno;

    if( socket_fd == -1 ){
        printf("failed to get socket(%d)\n", errno_save);
        return 2;
    }

    // make addr info
    address_destination.sin_family = AF_INET;
    address_destination.sin_port = htons(atoi(argv[2]));
    address_destination.sin_addr.s_addr = inet_addr(argv[1]);

    // send packet
    function_returned_number = sendto( socket_fd, argv[3], sizeof(argv[3]), 0, (struct sockaddr *)&address_destination, sizeof(address_destination));
    errno_save = errno;
    
    if( function_returned_number == -1 ){
        printf("failed to send(%d)\n", errno_save);
        return 3;
    }

    close(socket_fd);

    return 0;
}
