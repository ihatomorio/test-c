#include <stdio.h>

#include "udp6_ll.h"

int main(int argc, char const *argv[])
{
    int mysend_returns;

    mysend_returns = my_send();

    return mysend_returns;
}

//sent packet
// 23:28:29.904542 08:00:27:cc:29:65 > ff:ff:ff:ff:ff:ff,
// ethertype IPv6 (0x86dd), length 66: 
// (hlim 255, next-header UDP (17) payload length: 12)
// 2001:db8::214:51ff:fe2f:1556.4950 > 2404:6800:4004:80d::200e.4950: 
// [udp sum ok] UDP, length 4
//
//                                                +ethertype IPv6 (0x86dd)
//                                                |    + Version: 6 (4bit)
//                                                |    |+Traffic Class (8bit)
//                                                |    || + Flow Label(16 + 4bit)
//                  +  dest MAC  + + source MAC + +--. |+.+
//         0x0000:  ffff ffff ffff 0800 27cc 2965 86dd 6000  ........'.)e..`.
//
//                       + Payload Length (16bit) length 4
//                       |    +Next header
//                       |    | +Hop Lim
//                       |    | |  +source Addr (128bit) 2001:db8::214:51ff:fe2f:1556
//                  ---. +--. +.+. +-----------------------
//         0x0010:  0000 000c 11ff 2001 0db8 0000 0000 0214  ................
//
//                                 +Dest Addr (128bit) 2404:6800:4004:80d::200e
//                  -------------. +-----------------------
//         0x0020:  51ff fe2f 1556 2404 6800 4004 080d 0000  Q../.V$.h.@.....
//
//                                 +src port 4950(=0x1356)
//                                 |    +dest port 4950(=0x1356)
//                                 |    |    +segment len
//                                 |    |    |    +checksum
//                  -------------. +--. +--. +--. +--. +--- Data(4byte)
//         0x0030:  0000 0000 200e 1356 1356 000c 87da 5465  .......V.V....Te
//
//                  ---. data end
//         0x0040:  7374                                     st