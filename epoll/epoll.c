#include <stdio.h>
#include <sys/epoll.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

void accept_loop(int soc)
{
    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
    struct sockaddr_storage from;
    int acc, count, i, epollfd, nfds, ret;
    socklen_t len;
    struct epoll_event ev, events[MAX_CHILD];

    if ((epollfd = epoll_create(MAX_CHILD + 1)) == -1)
    {
        perror("epoll_create");
        return;
    }
    /* EPOLL用データの作成 */
    ev.data.fd = soc;
    ev.events = EPOLLIN;
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, soc, &ev) == -1)
    {
        perror("epoll_ctl");
        (void)close(epollfd);
        return;
    }
    count = 0;
    for (;;)
    {
        (void)fprintf(stderr, "<<child count:%d>>\n", count);
        switch ((nfds = epoll_wait(epollfd, events, MAX_CHILD + 1, 10 * 1000)))
        {
        case -1:
            /* エラー */
            perror("epoll_wait");
            break;
        case 0:
            /* タイムアウト */
            break;
        default:
            /* ソケットがレディ */
            for (i = 0; i < nfds; i++)
            {
                /* events[i].data.fdは全て読み込み可能 */
                if (events[i].data.fd == soc)
                {
                    /* サーバソケットレディ */
                    len = (socklen_t)sizeof(from);
                    /* 接続受付 */
                    if ((acc = accept(soc, (struct sockaddr *)&from, &len)) == -1)
                    {
                        if (errno != EINTR)
                        {
                            perror("accept");
                        }
                    }
                    else
                    {
                        (void)getnameinfo((struct sockaddr *)&from, len,
                                          hbuf, sizeof(hbuf),
                                          sbuf, sizeof(sbuf),
                                          NI_NUMERICHOST | NI_NUMERICSERV);
                        (void)fprintf(stderr, "accept:%s:%s\n", hbuf, sbuf);
                        /* 空きが無い */
                        if (count + 1 >= MAX_CHILD)
                        {
                            /* これ以上接続できない */
                            (void)fprintf(stderr,
                                          "connection is full : cannot accept\n");
                            /* クローズしてしまう */
                            (void)close(acc);
                        }
                        else
                        {
                            ev.data.fd = acc;
                            ev.events = EPOLLIN;
                            if (epoll_ctl(epollfd, EPOLL_CTL_ADD, acc, &ev) == -1)
                            {
                                perror("epoll_ctl");
                                (void)close(acc);
                                (void)close(epollfd);
                                return;
                            }
                            count++;
                        }
                    }
                }
                else
                {
                    /* 送受信 */
                    if ((ret = send_recv(events[i].data.fd, events[i].data.fd)) == -1)
                    {
                        /* エラーまたは切断 */
                        if (epoll_ctl(epollfd,
                                      EPOLL_CTL_DEL,
                                      events[i].data.fd,
                                      &ev) == -1)
                        {
                            perror("epoll_ctl");
                            (void)close(acc);
                            (void)close(epollfd);
                            return;
                        }
                        /* クローズ */
                        (void)close(events[i].data.fd);
                        count--;
                    }
                }
            }
            break;
        }
    }
    (void)close(epollfd);
}